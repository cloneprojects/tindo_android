package com.pyramidions.tindo.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pyramidions.tindo.R;


public class ViewPagerFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_view_pager, container, false);

        ImageView backgroundImage =  v.findViewById(R.id.backgroundImage);
//        pageText.setText(getArguments().getString("title"));
        Glide.with(getActivity()).load(getArguments().getString("background")).into(backgroundImage);

        return v;
    }

    public static ViewPagerFragment newInstance( String background) {

        ViewPagerFragment f = new ViewPagerFragment();
        Bundle b = new Bundle();

        b.putString("background", background);

        f.setArguments(b);

        return f;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser)
        {

        }
        else{

        }
    }

}
