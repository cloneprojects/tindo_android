package com.pyramidions.tindo.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.pyramidions.tindo.Helper.AppSettings;
import com.pyramidions.tindo.Helper.ReadFiles;
import com.pyramidions.tindo.Helper.UrlHelper;
import com.pyramidions.tindo.Helper.Utils;
import com.pyramidions.tindo.R;
import com.pyramidions.tindo.Volley.ApiCall;
import com.pyramidions.tindo.Volley.VolleyCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PhoneNumberActivity extends AppCompatActivity {
    Button nextButton;
    ImageView backgroundImage;
    EditText phoneNumber;
    Spinner countries;
    private ArrayList<String> phone_code;
    private ArrayList<String> countryName;
    ImageView backButton;
    String type;
    AppSettings appSettings = new AppSettings(PhoneNumberActivity.this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_phone_number);
        parseCountryCodes();
        initViews();
        initListeners();
        initAdapters();
    }

    public ArrayList<String> parseCountryCodes() {
        String response = "";
        ArrayList<String> list = new ArrayList<String>();
        phone_code = new ArrayList<String>();
        countryName = new ArrayList<String>();

        try {
            response = ReadFiles.readRawFileAsString(this,
                    R.raw.countrycodes);

            JSONArray array = new JSONArray(response);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                //list.add(object.getString("alpha-2") + " (" + object.getString("phone-code") + ")");
                list.add(object.getString("name"));
                countryName.add(object.getString("name"));
                phone_code.add(object.getString("name") + "(" + object.getString("phone-code") + ")");

            }
            //Collections.sort(list);
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return phone_code;
    }

    private void initAdapters() {
        ArrayAdapter<String> countryCodeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, parseCountryCodes());

//        CountryCodeAdapter countryCodeAdapter=new CountryCodeAdapter(PhoneNumberActivity.this,phone_code,countryName);
        countries.setAdapter(countryCodeAdapter);

        countries.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    Utils.hideKeyboard(PhoneNumberActivity.this);

                }
                return false;
            }
        });

        countries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

//                LinearLayout linearLayout= (LinearLayout) countries.getSelectedView();

                try {
                    TextView countryName = (TextView) countries.getSelectedView();
                    countryName.setTextColor(getResources().getColor(R.color.white));
                } catch (Exception e) {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }


    private void initListeners() {
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    regPhoneNumber();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void moveVerificationActivity(String OTP, String userStatus) {
        Intent intent = new Intent(PhoneNumberActivity.this, OTPVerficationActivity.class);
        intent.putExtra("otp", OTP);
        intent.putExtra("mobile_number", phoneNumber.getText().toString());
        intent.putExtra("userStatus", userStatus);
        appSettings.setLoginType(type);
        startActivity(intent);
    }

    private void initViews() {

        nextButton = (Button) findViewById(R.id.nextButton);
        backButton = (ImageView) findViewById(R.id.backButton);
        countries = (Spinner) findViewById(R.id.country_spinner);
        phoneNumber = (EditText) findViewById(R.id.phoneNumber);
        backgroundImage = (ImageView) findViewById(R.id.backgroundImage);
//        Glide.with(PhoneNumberActivity.this).load("http://104.131.74.144/images/background_phone_number.jpg").into(backgroundImage);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        type = appSettings.getLoginType();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void regPhoneNumber() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("mobileNumber", phoneNumber.getText().toString());
        ApiCall.PostMethod(PhoneNumberActivity.this, UrlHelper.REGISTER, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                String OTP = response.optString("randomotp");
                String UserStatus = response.optString("UserStatus");
                if (response.has("senderID")) {
                    appSettings.setUserId(response.optString("senderID"));

                }
                moveVerificationActivity(OTP, UserStatus);


            }
        });
    }
}
